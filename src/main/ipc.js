export const sendStepMessage = (mainWindow, msg) => {
  mainWindow.webContents.send("step", msg);
};

export const sendIgnoreMouseMessage = (mainWindow, msg) => {
  mainWindow.webContents.send("ignoreMouse", msg);
};

export const sendHideMapSeeds = (mainWindow, msg) => {
  mainWindow.webContents.send("hideMapSeeds", msg);
};

export const sendInitialMessages = (
  mainWindow,
  { step, isDragHidden, isMapSeedsHidden }
) => {
  sendStepMessage(mainWindow, step);
  sendIgnoreMouseMessage(mainWindow, isDragHidden);
  sendHideMapSeeds(mainWindow, isMapSeedsHidden);
};
