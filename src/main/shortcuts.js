import { globalShortcut } from "electron";
import {
  store,
  toggleDrag,
  prevStep,
  nextStep,
  toggleIsMapSeedsHidden,
} from "./store";
import {
  sendStepMessage,
  sendIgnoreMouseMessage,
  sendHideMapSeeds,
} from "./ipc";

export const registerGlobalShortcuts = (mainWindow) => {
  globalShortcut.register("CommandOrControl+W", () => {
    const ignoreMouse = toggleDrag(store);
    mainWindow.setIgnoreMouseEvents(ignoreMouse);
    sendIgnoreMouseMessage(mainWindow, ignoreMouse);
  });

  globalShortcut.register("CommandOrControl+A", () => {
    sendStepMessage(mainWindow, prevStep(store));
  });

  globalShortcut.register("CommandOrControl+D", () => {
    sendStepMessage(mainWindow, nextStep(store));
  });

  globalShortcut.register("CommandOrControl+F1", () => {
    const hideMapSeeds = toggleIsMapSeedsHidden(store);
    sendHideMapSeeds(mainWindow, hideMapSeeds);
  });
};
