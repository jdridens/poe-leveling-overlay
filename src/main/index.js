import { app, BrowserWindow } from "electron";
import * as path from "path";
import { format as formatUrl } from "url";
import { store, getInitialStore } from "./store";
import { registerGlobalShortcuts } from "./shortcuts";
import { sendInitialMessages } from "./ipc";

const isDevelopment = process.env.NODE_ENV !== "production";
const INDEX_HTML = path.join(__dirname, "index.html");

let mainWindow;

function createMainWindow() {
  const window = new BrowserWindow({
    width: 1000,
    webPreferences: { nodeIntegration: true },
    transparent: true,
    frame: false,
    alwaysOnTop: true,
    resizable: false,
    maximizable: false,
    minimizable: false,
    hasShadow: false,
  });

  window.setAlwaysOnTop(true, "pop-up-menu");

  // window.webContents.openDevTools();

  if (isDevelopment) {
    window.loadURL(`http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}`);
  } else {
    window.loadURL(
      formatUrl({
        pathname: INDEX_HTML,
        protocol: "file",
        slashes: true,
      })
    );
  }

  window.on("closed", () => {
    mainWindow = null;
  });

  return window;
}

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    mainWindow = createMainWindow();
  }
});

app.whenReady().then(() => {
  mainWindow = createMainWindow();

  registerGlobalShortcuts(mainWindow);

  mainWindow.webContents.once("dom-ready", () => {
    sendInitialMessages(mainWindow, getInitialStore(store));
  });
});
