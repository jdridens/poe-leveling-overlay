import Store from "electron-store";

const IS_MAP_SEEDS_HIDDEN = "isMapSeedsHidden";
const IS_DRAG_HIDDEN = "isDragHidden";
const STEP = "step";

export const store = new Store({
  [STEP]: {
    type: "number",
    minimum: 0,
    default: 0,
  },
  [IS_DRAG_HIDDEN]: {
    type: "boolean",
    default: true,
  },
  [IS_MAP_SEEDS_HIDDEN]: {
    type: "boolean",
    default: false,
  },
});

export const getInitialStore = (store) => {
  return {
    [STEP]: store.get(STEP),
    [IS_DRAG_HIDDEN]: store.get(IS_DRAG_HIDDEN),
    [IS_MAP_SEEDS_HIDDEN]: store.get(IS_MAP_SEEDS_HIDDEN),
  };
};

export const toggleIsMapSeedsHidden = (store) => {
  const isMapSeedsHidden = store.get(IS_MAP_SEEDS_HIDDEN);
  return isMapSeedsHidden ? showMapSeeds(store) : hideMapSeeds(store);
};

export const showMapSeeds = (store) => {
  store.set(IS_MAP_SEEDS_HIDDEN, false);
  return false;
};

export const hideMapSeeds = (store) => {
  store.set(IS_MAP_SEEDS_HIDDEN, true);
  return true;
};

export const toggleDrag = (store) => {
  const isDragHidden = store.get(IS_DRAG_HIDDEN);
  return isDragHidden ? showDrag(store) : hideDrag(store);
};

export const hideDrag = (store) => {
  store.set(IS_DRAG_HIDDEN, true);
  return true;
};

export const showDrag = (store) => {
  store.set(IS_DRAG_HIDDEN, false);
  return false;
};

export const nextStep = (store) => {
  const storeStep = store.get(STEP);
  const step = storeStep + 1;
  store.set(STEP, step);
  return step;
};

export const prevStep = (store) => {
  const storeStep = store.get(STEP);
  const step = storeStep === 0 ? 0 : storeStep - 1;
  store.set(STEP, step);
  return step;
};
