import React from "react";
import Drag from "./Drag";
import Steps from "./Steps";
import MapSeed from "./MapSeed";

const App = () => (
  <>
    <Drag />
    <MapSeed />
    <Steps />
  </>
);

export default App;
