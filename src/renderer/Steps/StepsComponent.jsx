import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const Text = styled.p`
  pointer-events: none;
  color: white;
  font-size: 18px;
  letter-spacing: 1px;
  font-family: Arial, Helvetica, sans-serif;
  text-shadow: -2px 1px 0 #000, 1px 1px 0 #000, 1px -1px 0 #000,
    -1px -1px 0 #000;
`;

const StepsRenderer = ({ step }) => {
  return <Text>{step}</Text>;
};

StepsRenderer.propTypes = {
  step: PropTypes.string.isRequired,
};

export default React.memo(StepsRenderer);
