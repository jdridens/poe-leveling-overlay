import React from "react";
import fs from "fs";
import path from "path";
import { ipcRenderer } from "electron";
import StepsComponent from "./StepsComponent";

const StepsContainer = () => {
  const [step, setStep] = React.useState(null);

  React.useEffect(() => {
    const pathToSteps = path.join(__static, "/steps-zone.json");
    const fileContents = fs.readFileSync(pathToSteps, "utf8");
    const stepsJson = JSON.parse(fileContents);

    ipcRenderer.on("step", (e, idx) => {
      setStep(stepsJson[idx].inst);
    });
  }, []);

  return step ? <StepsComponent step={step} /> : null;
};
export default StepsContainer;
