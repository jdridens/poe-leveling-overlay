import React from "react";
import fs from "fs";
import path from "path";
import { ipcRenderer } from "electron";
import { map, filter, contains, equals } from "ramda";
import MapSeedComponent from "./MapSeedComponent";
import { getStatic } from "../getStatic";

const MapSeedContainer = () => {
  const [imgSrcPaths, setImgSrcPaths] = React.useState([]);
  const [isHidden, setIsHidden] = React.useState(false);
  React.useEffect(() => {
    const pathToSteps = path.join(__static, "/steps-zone.json");
    const fileContents = fs.readFileSync(pathToSteps, "utf8");
    const stepsJson = JSON.parse(fileContents);

    ipcRenderer.on("hideMapSeeds", (e, val) => {
      setIsHidden(val);
    });

    ipcRenderer.on("step", (e, idx) => {
      const { act, zone } = stepsJson[idx];
      if (equals("none", zone)) {
        setImgSrcPaths([]);
        return;
      }
      const actImgPath = path.join(__static, "imgs", `act${act}`);
      fs.readdir(actImgPath, (err, files) => {
        if (err) {
          console.log(err);
          return;
        }
        const zoneImgs = filter(contains(zone), files);
        const zoneImgPaths = map(
          (img) => getStatic(path.join("imgs", `act${act}`, img)),
          zoneImgs
        );
        setImgSrcPaths(zoneImgPaths);
      });
    });
  }, []);

  return isHidden ? null : <MapSeedComponent imgSrcPaths={imgSrcPaths} />;
};

export default MapSeedContainer;
