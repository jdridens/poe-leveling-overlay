import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { map } from "ramda";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
`;

const Img = styled.img`
  padding: 2px;
  width: 180px;
`;

const MapSeedComponent = ({ imgSrcPaths }) => {
  return (
    <Container>
      {map(
        (imgSrc) => (
          <Img src={imgSrc} key={imgSrc} />
        ),
        imgSrcPaths
      )}
    </Container>
  );
};

MapSeedComponent.propTypes = {
  imgSrcPaths: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default React.memo(MapSeedComponent);
