import React from "react";
import { ipcRenderer } from "electron";
import DragComponent from "./DragComponent";

const DragContainer = () => {
  const [isHidden, setIsHidden] = React.useState(true);
  React.useEffect(() => {
    ipcRenderer.on("ignoreMouse", (e, val) => {
      setIsHidden(val);
    });
  }, []);
  return <DragComponent hidden={isHidden} />;
};

export default DragContainer;
