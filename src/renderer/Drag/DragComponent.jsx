import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const Drag = styled.div`
  -webkit-app-region: drag;
  width: 10px;
  height: 10px;
  background-color: sandybrown;
  color: white;
`;

const DragRenderer = ({ hidden }) => {
  return hidden ? null : <Drag />;
};

DragRenderer.propTypes = {
  hidden: PropTypes.bool.isRequired,
};

export default React.memo(DragRenderer);
